/*
 * Grunt HTML Emails
 * https://bitbucket.org/account/signin/?next=/dlearious/rate-git/
 *
 * Copyright (c) 2013 Dominic Lear
 * Licensed under the MIT license.
 */

module.exports = function(grunt) {

	// Some core variables and commmand line flags
	var baseUri   =  'converted-html/',
	    exportUri =  'dist/',
	    transExportUri = 'trans/',
	    inputFile =  grunt.option('F') || grunt.option('file') || 'index.html',
	    outputTempFile = inputFile.split("/")[1];
	    outputFile = exportUri + outputTempFile;
	    transOutputFile = transExportUri + outputTempFile;
	var csspaths = '';
	var baseUriMinus = '/converted-html';

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'), // Load in variables from pacakge.json

		// Modify css in the head
		dom_munger: {
			your_target: {
				options: {
					//Read css that needs to be inlined					
					callback: function($){
						fs = require('fs');

						// Find and insert css files into the <head>
						var $headCss = $('head').find('link[data-premailer="ignore"]');
						$headCss.each(function( index ) {
							var href = $(this).attr('href');
							var data = fs.readFileSync(baseUri + href.substring(2,1000),'utf8');
							$('head').append('<style  data-premailer="ignore">' + data + '</style>');
						});

						// Remove all links from head
						$('head').find('link').remove();

						// Add Outlook css to head
						var outlookCss = fs.readFileSync(baseUri + 'assets/css/outlook.css','utf8');
						$('head').append('<!--[if gte mso 9]><style type="text/css" data-premailer="ignore">' + outlookCss + '</style><![endif]-->');
						
						// Make sure core images are adjusted
						$('img').each(function(index){
						    var imgSrc = $(this).attr('src');  
						    if (imgSrc.substring(0,2) == '..') {
						        imgSrc = "http://media.ratemyplacement.co.uk.s3.amazonaws.com/email-build-001" + imgSrc.substring(2,1000);
						    }
						    $(this).attr('src', imgSrc);
						});

						// Find cta buttons and insert links as a replacment for outlook due no support for display: inline-block; 
						$('.cta').each(function(index) {
							var $ctaTarget = $(this).find('a');
							var linkText = $ctaTarget.text();
							var linkHref = $ctaTarget.attr('href');
							$(this).append(
								'<!--[if mso]><a href="' + 
								linkHref + 
								'" style="color:#3CA7DD; font-weight:bold; text-decoration: none; font-family: HelveticaNeue, Helvetica, Arial, sans-serif;">' + 
								linkText + 
								'</a><![endif]-->'
							);							 
						});
					}
				},
				src:  baseUri + inputFile,
				dest: outputFile
			},
		},

		// Inline the css and create text version
		premailer: {
			html: {
				options: {
					verbose: false,
					css: ['converted-html/assets/css/main.css']
					//baseUrl: 'http://media.ratemyplacement.co.uk.s3.amazonaws.com/email-build-001/'
				},
				files: {
					'dist/temp.html': [outputFile]
				}
			},
			text: {
				options: {
					verbose: false,
					mode: 'txt'
				},
				files: {
					'dist/temp.txt': [outputFile]
				}
			}
		},

		// Test the email by sending
		nodemailer: {
			test: {
				options: {
					transport: {
						type: 'SMTP',
						options: {
							service: 'Gmail',
							auth: {
								user: '<%= pkg.emailUser %>',
								pass: '<%= pkg.emailPass %>'
							},
							from: 'dev <dev@dominiclear.com>'
						}
					},
					recipients: '<%= pkg.recipients %>',
					subject: outputFile + ' <%= pkg.subject %>'
				},
				src: ['dist/temp.html', 'dist/temp.txt']
			}
		},

		// Rename temporary file to original file name
		rename: {
	        html: {
	            src: 'dist/temp.html',
	            dest: outputFile
	        },
	        text: {
	            src: 'dist/temp.txt',
	            dest: outputFile.split('.')[0] + '.txt' // Do the same for the text file
	        },
	        transhtml: {
	            src: 'dist/temp.html',
	            dest: transOutputFile
	        },
	        transtext: {
	            src: 'dist/temp.txt',
	            dest: transOutputFile.split('.')[0] + '.txt' // Do the same for the text file
	        }	        
   		},

   		// Prettify file for easier editing later
		prettify: {
			options: {
			    indent: 1,
			    indent_char: ' ',
			    brace_style: 'collapse',
			    unformatted: ['a', 'sub', 'sup', 'b', 'i', 'u', 'strong', 'em', 'span']
			},			
			default: {
				src: outputFile,
				dest: outputFile
			},
			trans: {
				src: transOutputFile,
				dest: transOutputFile
			}
		}



	})

	// Load all required modules
	grunt.loadNpmTasks('grunt-dom-munger');	
	grunt.loadNpmTasks('grunt-premailer');	
	grunt.loadNpmTasks('grunt-nodemailer');
	grunt.loadNpmTasks('grunt-rename');
	grunt.loadNpmTasks('grunt-prettify');

	// Tasks available
	grunt.registerTask('default', ['dom_munger', 'premailer:html','premailer:text', 'rename:html', 'rename:text', 'prettify:default']); // Just build the html files
	grunt.registerTask('test', ['dom_munger', 'premailer:html','premailer:text', 'nodemailer:test', 'rename:html', 'rename:text', 'prettify:default']); // Build and test the email
	grunt.registerTask('trans', ['dom_munger', 'premailer:html','premailer:text', 'nodemailer:test', 'rename:transhtml', 'rename:transtext', 'prettify:trans']);
};

